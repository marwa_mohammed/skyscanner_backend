'use strict';
var fs = require('fs');
const express = require('express')
const cors = require('express-cors');
var app = express();

var seats_webhook = require('./routes/seats');


app.get('/seats', seats_webhook);

module.exports = app;