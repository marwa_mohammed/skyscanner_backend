'use strict';
const app = require('./app');

var express = require('express');
var router = express.Router();

app.get('/', (req, res) => res.send('Hello World!'));

module.exports = router;